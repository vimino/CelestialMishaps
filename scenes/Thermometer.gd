extends Sprite

var update = false
var speed = 1 # Reverse movement speed (> 0)
var current = 1000 # Current measure (1000 = empty)
var target = 1000 # Target measure (1000 = empty)
var stop = false

func getCurrent():
	return (100 - (current / 10))

func setCurrent(value): # Value between [0, 100]
	if value < 0:
		value = 0
	if value > 100:
		value = 100
	current = 1000 - 10 * value
	target = current
	update = true

func getTarget():
	return (100 - (target / 10))

func setTarget(value = null): # Value between [0, 100]
	if value == null:
		target = current
		update = true
		return

	if value < 0:
		value = 0
	if value > 100:
		value = 100
	target = 1000 - 10 * value
	update = true

func setSpeed(value): # Value between [0, 100]
	if value < 0:
		value = 0
	if value > 100:
		value = 100
	speed = 1 + (100 - value)
	
func stop():
	stop = true
	
func restart():
	stop = false

func _process(delta):
	if stop:
		return
		
	if update or current != target:
		var diff = target - current
		if diff > -1 and diff < 1:
			current = target
		else:
			current += (diff / speed) * delta
		if current >= 1000:
			current = 1000
			#target = 0
		elif current <= 0:
			current = 0
			#target = 1000
		$Meter.region_rect = Rect2(0, current, 250, 1000 - current)
		$Meter.position.y = current

		update = false