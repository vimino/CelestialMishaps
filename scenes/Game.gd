extends Node2D

var gameOver = false
export var level = 1
var previous = 0
var celestial = 0 # 0 = None, 1 = Sun, 2 = Moon
var angle = 135 # [135-140]
export var speed = 1
export var strength = 2 # Percent of the Value that is changed
var skip = false
var updated = false
var grab = false
var yOffset = 900

func getPosition(angle, delta):
	if grab:
		yOffset -= 1000 * delta
		
	return Vector2(320 + 640 * cos(angle), yOffset + 640 * sin(angle))
	
func restart():
	$GameOver.visible = false
	$Planet/Burnt.visible = false
	$Planet/Frozen.visible = false
	gameOver = false
	randomize()
	# Set initial values
	$Thermometer.setSpeed(100) # [0,100] % of the whole change
	$Thermometer.setCurrent(50)
	$Thermometer.restart()
	# Reset all globals and scales
	level = 1
	speed = 1
	strength = 2
	$Sun.scale = Vector2(0.3, 0.3)
	$Moon.scale = Vector2(0.25, 0.25)
	nextLevel()

func _ready():
	self.set_process(true)
	self.set_process_unhandled_input(false)
	restart()

func nextLevel():
	if gameOver:
		return
		
	$Thermometer.setTarget()
	$Sun.visible = false
	$Moon.visible = false
	yOffset = 900
	angle = 135
	updated = false
	
	celestial = randi()%3
	while previous == 0 and celestial == 0: # never wait twice
		celestial = randi()%3
	previous = celestial

	level += 1
	if strength > 0.02:
		strength -= 0.0001
	if speed < 3:
		speed += 0.001
	#print('LVL ', level)
	#speed = level * 0.2
	var value = $Sun.scale.x
	if value < 1:
		value += 0.01
		$Sun.scale = Vector2(value, value)
		$Moon.scale = Vector2(value - 0.05, value - 0.05)

func timeout():
	# $Timer.queue_free() # musn't free the timer as we will reuse it
	nextLevel()
	skip = false
	
func _input(event):
	if (event is InputEventKey or event is InputEventMouseButton or event is InputEventScreenTouch):
		if gameOver:
			restart()
		
		if event.is_pressed():
			if grab == null:
				grab = false
		else:
			if grab == false:
				grab = true

func _process(delta):
	if $Thermometer.current <= 30 or $Thermometer.current >= 970: # Game Over
		GameOver()
		return
		
	if skip or gameOver:
		return
		

	if celestial == 0: # None, just wait
		$Thermometer.setTarget()
		$Timer.set_wait_time(1) #rand_range(1, 3))
		$Timer.start()
		skip = true
		yield($Timer, 'timeout')
	elif celestial == 1: # Hot
		if not $Sun.visible:
			$Sun.visible = true
			$Moon.visible = false
			
		$Sun.position = getPosition(angle, delta)
		angle += speed * delta
		
		if yOffset < 300: # Took the Celestial down
			$Thermometer.setTarget() # Set the Target to the Current
			grab = null
			updated = true
			nextLevel()
			return
		
		if not updated and angle > 137:
			var value = $Thermometer.getCurrent()
			$Thermometer.setTarget(value + (strength * level))
			#print($Thermometer.getTarget(), '+', strength * value * level, ',', $Thermometer.current, ',', $Thermometer.target)
			updated = true
			
		if angle > 140:
			nextLevel()
	elif celestial == 2: # Cold
		if not $Moon.visible:
			$Sun.visible = false
			$Moon.visible = true
			
		$Moon.position = getPosition(angle, delta)
		angle += speed * delta
		
		if yOffset < 300: # Took the Celestial down
			$Thermometer.setTarget() # Set the Target to the Current
			grab = null
			updated = true
			nextLevel()
			return
		
		if not updated and angle > 137:
			var value = $Thermometer.getCurrent()
			$Thermometer.setTarget(value - (strength * level))
			#print($Thermometer.getTarget(), '-', strength * value * level, ',', $Thermometer.current, ',', $Thermometer.target)
			updated = true
			
		if angle > 140:
			nextLevel()

func GameOver():
	gameOver = true
	$Sun.visible = false
	$Moon.visible = false
	$GameOver.visible = true
	# print('Game Over at ', $Thermometer.current)
	if $Thermometer.current < 500:
		$Planet/Burnt.visible = true
	else:
		$Planet/Frozen.visible = true
	$Thermometer.stop()
