extends Sprite

var increaseFace = true

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# Rotate the Moon
	self.rotation_degrees += (80 * delta)
	if self.rotation_degrees > 360:
		self.rotation_degrees -= 360
		
	# Scale the Face
	var newScale = $Face.scale.x
	if increaseFace:
		newScale += 0.5 * delta
	else:
		newScale -= 0.5 * delta
	$Face.scale.x = newScale
	$Face.scale.y = newScale
		
	if newScale < 0.8:
		increaseFace = true
	elif newScale > 1.0:
		increaseFace = false
