extends Sprite

var increaseFace = true

func _process(delta):
	# Rotate the Sun
	self.rotation_degrees += (100 * delta)
	if self.rotation_degrees > 360:
		self.rotation_degrees -= 360
		
	# Scale the Face
	var newScale = $Face.scale.x
	if increaseFace:
		newScale += 1.5 * delta
	else:
		newScale -= 1.5 * delta
	$Face.scale.x = newScale
	$Face.scale.y = newScale
		
	if newScale < 0.8:
		increaseFace = true
	elif newScale > 1.2:
		increaseFace = false