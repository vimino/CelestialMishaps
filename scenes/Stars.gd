extends Sprite

var speed = 5

func setSpeed(value):
	speed = value
	if speed < 0:
		speed = 0
	if speed > 100:
		speed = 100

func _process(delta):
	self.rotation_degrees += (speed * delta)
	if self.rotation_degrees > 360:
		self.rotation_degrees -= 360